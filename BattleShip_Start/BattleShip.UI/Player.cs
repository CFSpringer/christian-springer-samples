﻿using BattleShip.BLL.GameLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleShip.UI
{
    public class Player
    {
        public string PlayerName { get; set; }
        public char[,] DisplayBoard { get; set; }
        public Board PlayBoard {get; set;}

        public Player()
        {
        }
    }
}
