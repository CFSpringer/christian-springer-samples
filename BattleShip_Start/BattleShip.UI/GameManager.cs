﻿using BattleShip.BLL.GameLogic;
using BattleShip.BLL.Requests;
using BattleShip.BLL.Responses;
using BattleShip.BLL.Ships;
using BattleShip.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleShip.UI
{
    public class GameManager
    {
        public string p1name;
        public string p2name;
        Player p1 = new Player();
        Player p2 = new Player();

        public void PlayTheGame()
        {
            GreetPlayers();
            GetPlayerNames();
            bool keepPlaying;
            do
            {
                p1.PlayBoard = new Board();
                p2.PlayBoard = new Board();
                p1.DisplayBoard = new char[10, 10];
                p2.DisplayBoard = new char[10, 10];
                Console.WriteLine($"{p2.PlayerName} Please GO AWAY while {p1.PlayerName} places their ships.");
                Console.WriteLine($"{p1.PlayerName} Press any key to continue ");
                Console.ReadKey();
                PlaceShips(p1);
                Console.WriteLine($"Please GO AWAY while {p2.PlayerName} places their ships.");
                Console.WriteLine($"{p2.PlayerName} Press any key to continue and place your ships.");
                Console.ReadKey();
                PlaceShips(p2);
                keepPlaying = GamePlay();
            }
            while (keepPlaying);

        }

        public void GreetPlayers()
        {
            Console.WriteLine("Welcome to Battleship!");
            Console.WriteLine("Battleship is a competitive game for 2 players.");
            Console.WriteLine("Press any key to continue");
            Console.ReadKey();

        }

        public void GetPlayerNames()
        {
            Console.Clear();
            while (true)
            {
                Console.WriteLine($"Player 1, please enter your name.");
                p1name = Console.ReadLine();
                if (string.IsNullOrEmpty(p1name))
                {
                    Console.WriteLine("Please enter a name.");
                    continue;
                }
                p1.PlayerName = p1name;
                break;
            }
            while (true)
            {
                Console.WriteLine($"Player 2, please enter your name.");
                p2name = Console.ReadLine();
                if (string.IsNullOrEmpty(p2name))
                {
                    Console.WriteLine("Please enter a name.");
                    continue;
                }
                p2.PlayerName = p2name;
                break;
            }
        }

        public static void DrawInitialGrid(char[,] grid)
        {
            Console.Clear();
            string topBar = "  ╔═══╦═══╦═══╦═══╦═══╦═══╦═══╦═══╦═══╦═══╗";
            string bottomBar = "  ╚═══╩═══╩═══╩═══╩═══╩═══╩═══╩═══╩═══╩═══╝";
            Console.WriteLine("\n    A   B   C   D   E   F   G   H   I   J");
            Console.WriteLine(topBar);
            for (int i = 0; i < 10; i++)
            {
                if (i == 0)
                {
                    Console.Write(i + 1);
                }
                if (i > 0 && i <= 9)
                {
                    Console.WriteLine($"\n  ╠═══╬═══╬═══╬═══╬═══╬═══╬═══╬═══╬═══╬═══╣");
                    Console.Write(i + 1);
                }

                for (int j = 0; j < 10; j++)
                {
                    if (i < 9)
                    {
                        Console.Write($" ║{grid[j, i] = ' '} ");
                    }
                    else
                    {
                        Console.Write($"║{grid[j, i] = ' '}  ");
                        if (j == 9)
                        {
                            Console.Write("║");
                        }
                    }
                }
                if (i < 9)
                {
                    Console.Write(" ║");
                }
            }
            Console.WriteLine($"\n{bottomBar}");
        }

        public static void DrawGrid(char[,] grid)
        {
            Console.Clear();
            string topBar = "  ╔═══╦═══╦═══╦═══╦═══╦═══╦═══╦═══╦═══╦═══╗";
            string bottomBar = "  ╚═══╩═══╩═══╩═══╩═══╩═══╩═══╩═══╩═══╩═══╝";
            Console.WriteLine("\n    A   B   C   D   E   F   G   H   I   J");
            Console.WriteLine(topBar);
            for (int i = 0; i < 10; i++)
            {
                if (i == 0)
                {
                    Console.Write(i + 1);
                }
                if (i > 0 && i <= 9)
                {
                    Console.WriteLine($"\n  ╠═══╬═══╬═══╬═══╬═══╬═══╬═══╬═══╬═══╬═══╣");
                    Console.Write(i + 1);
                }

                for (int j = 0; j < 10; j++)
                {
                    if (i < 9)
                    {
                        Console.Write($" ║ ");
                        if (grid[j, i] == 'M')
                        {
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.Write(grid[j, i]);
                            Console.ResetColor();
                        }
                        else if (grid[j, i] == 'H')
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.Write(grid[j, i]);
                            Console.ResetColor();
                        }
                        else
                        {
                            Console.Write(grid[j, i]);
                        }

                    }
                    else
                    {
                        Console.Write($"║ ");
                        if (grid[j, i] == 'M')
                        {
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.Write($"{grid[j, i]} ");
                            Console.ResetColor();
                        }
                        else if (grid[j, i] == 'H')
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.Write($"{grid[j, i]} ");
                            Console.ResetColor();
                        }
                        else
                        {
                            Console.Write($"{grid[j, i]} ");
                        }
                        if (j == 9)
                        {
                            Console.Write("║");
                        }
                    }
                }
                if (i < 9)
                {
                    Console.Write(" ║");
                }
            }
            Console.WriteLine($"\n{bottomBar}");
        }

        //public Coordinate SelectCoordinate()

        //{
        //    //Console.Clear();
        //    string xSelection = "";
        //    string ySelection = "";
        //    bool validXCoordinate = false;
        //    Console.Write("Select an X-Coordinate (A-J)\n");
        //    while (!validXCoordinate)
        //    {
        //        xSelection = Console.ReadLine().ToUpper();
        //        switch (xSelection)
        //        {
        //            case "A":
        //            case "B":
        //            case "C":
        //            case "D":
        //            case "E":
        //            case "F":
        //            case "G":
        //            case "H":
        //            case "I":
        //            case "J":
        //                validXCoordinate = true;
        //                break;
        //            default:
        //                Console.Write("Select a VALID X-Coordinate (A-J)\n");
        //                continue;
        //        }
        //    }
        //    bool validYCoordinate = false;
        //    Console.Write("Please select a Y-Coordinate (1-10)\n");
        //    while (!validYCoordinate)
        //    {
        //        ySelection = Console.ReadLine().ToUpper();
        //        switch (ySelection)
        //        {
        //            case "10":
        //            case "1":
        //            case "2":
        //            case "3":
        //            case "4":
        //            case "5":
        //            case "6":
        //            case "7":
        //            case "8":
        //            case "9":
        //                validYCoordinate = true;
        //                break;
        //            default:
        //                Console.Write("Please select a VALID Y-Coordinate (1-10)\n");
        //                continue;
        //        }
        //    }
        //    char xChar = char.Parse(xSelection);
        //    int x = xChar - 64;
        //    int y = int.Parse(ySelection);
        //    Coordinate co = new Coordinate(y, x);
        //    return co;
        //}

        public ShipDirection SelectDirection()
        {
            string direction = "";
            bool validCoordinate = false;
            ShipDirection dir = new ShipDirection();
            Console.Write("Please select a direction for your ship to face (8-UP, 2-Down, 4-Left, 6-Right)\n");
            while (!validCoordinate)
            {
                direction = Console.ReadLine();
                switch (direction)
                {
                    case "8":
                        dir = ShipDirection.Up;
                        validCoordinate = true;
                        break;
                    case "2":
                        dir = ShipDirection.Down;
                        validCoordinate = true;
                        break;
                    case "4":
                        dir = ShipDirection.Left;
                        validCoordinate = true;
                        break;
                    case "6":
                        dir = ShipDirection.Right;
                        validCoordinate = true;
                        break;
                    default:
                        Console.WriteLine("Please Select a Valid Direction for your ship to: 8-Up, 2-Down, 4-Left, 6-Right");
                        continue;
                }
            }
            return dir;
        }

        public void PlaceShips(Player player)
        {
            bool destroyerPlaced = false;
            do
            {
                DrawGrid(player.DisplayBoard);
                PlaceShipRequest request = new PlaceShipRequest()
                {
                    Coordinate = SelectCoordinate(),
                    Direction = SelectDirection(),
                    ShipType = ShipType.Destroyer
                };
                var response = player.PlayBoard.PlaceShip(request);
                switch (response)
                {
                    case ShipPlacement.NotEnoughSpace:
                        Console.WriteLine("There is not enough space! Press any key to select new coordinates!");
                        Console.ReadLine();
                        break;
                    case ShipPlacement.Overlap:
                        Console.WriteLine("There is already a ship at that location! Press any key to try again");
                        Console.ReadLine();
                        break;
                    case ShipPlacement.Ok:
                        Console.WriteLine("Great Job... Your destroyer has been placed!");
                        Console.WriteLine("Get ready to place your submarine!");
                        Console.ReadLine();
                        Console.Clear();
                        destroyerPlaced = true;
                        break;
                    default:
                        continue;
                }
            } while (!destroyerPlaced);

            bool submarinePlaced = false;
            do
            {
                DrawGrid(player.DisplayBoard);
                PlaceShipRequest request = new PlaceShipRequest()
                {
                    Coordinate = SelectCoordinate(),
                    Direction = SelectDirection(),
                    ShipType = ShipType.Submarine
                };
                var response = player.PlayBoard.PlaceShip(request);
                switch (response)
                {
                    case ShipPlacement.NotEnoughSpace:
                        Console.WriteLine("There is not enough space! Please select new coordinates!");
                        Console.ReadLine();
                        break;
                    case ShipPlacement.Overlap:
                        Console.WriteLine("There is already a ship at that location!");
                        Console.ReadLine();
                        break;
                    case ShipPlacement.Ok:
                        Console.WriteLine("Great Job... Your submarine has been placed!");
                        Console.WriteLine("Get ready to place your cruiser!");
                        Console.ReadLine();
                        Console.Clear();
                        submarinePlaced = true;
                        break;
                    default:
                        continue;
                }
            } while (!submarinePlaced);

            bool cruiserPlaced = false;
            do
            {
                DrawGrid(player.DisplayBoard);
                PlaceShipRequest request = new PlaceShipRequest()
                {
                    Coordinate = SelectCoordinate(),
                    Direction = SelectDirection(),
                    ShipType = ShipType.Cruiser
                };
                var response = player.PlayBoard.PlaceShip(request);
                switch (response)
                {
                    case ShipPlacement.NotEnoughSpace:
                        Console.WriteLine("There is not enough space! Please select new coordinates!");
                        Console.ReadLine();
                        break;
                    case ShipPlacement.Overlap:
                        Console.WriteLine("There is already a ship at that location!");
                        Console.ReadLine();
                        break;
                    case ShipPlacement.Ok:
                        Console.WriteLine("Great Job... Your cruiser has been placed!");
                        Console.WriteLine("Get ready to place your  Battleship");
                        Console.ReadLine();
                        Console.Clear();
                        cruiserPlaced = true;
                        break;
                    default:
                        continue;
                }
            } while (!cruiserPlaced);

            bool battleshipPlaced = false;
            do
            {
                DrawGrid(player.DisplayBoard);
                PlaceShipRequest request = new PlaceShipRequest()
                {
                    Coordinate = SelectCoordinate(),
                    Direction = SelectDirection(),
                    ShipType = ShipType.Battleship
                };
                var response = player.PlayBoard.PlaceShip(request);
                switch (response)
                {
                    case ShipPlacement.NotEnoughSpace:
                        Console.WriteLine("There is not enough space! Please select new coordinates!");
                        Console.ReadLine();
                        break;
                    case ShipPlacement.Overlap:
                        Console.WriteLine("There is already a ship at that location!");
                        Console.ReadLine();
                        break;
                    case ShipPlacement.Ok:
                        Console.WriteLine("Great Job... Your Battleship has been placed!");
                        Console.WriteLine("Get ready to place your carrier!");
                        Console.ReadLine();
                        Console.Clear();
                        battleshipPlaced = true;
                        break;
                    default:
                        continue;
                }
            } while (!battleshipPlaced);

            bool carrierPlaced = false;
            do
            {
                DrawGrid(player.DisplayBoard);
                PlaceShipRequest request = new PlaceShipRequest()
                {
                    Coordinate = SelectCoordinate(),
                    Direction = SelectDirection(),
                    ShipType = ShipType.Carrier
                };
                var response = player.PlayBoard.PlaceShip(request);
                switch (response)
                {
                    case ShipPlacement.NotEnoughSpace:
                        Console.WriteLine("There is not enough space!  Please select new coordinates!");
                        Console.ReadLine();
                        break;
                    case ShipPlacement.Overlap:
                        Console.WriteLine("There is already a ship at that location!");
                        Console.ReadLine();
                        break;
                    case ShipPlacement.Ok:
                        Console.WriteLine("Great Job... Your carrier has been placed!");
                        Console.ReadLine();
                        Console.Clear();
                        carrierPlaced = true;
                        break;
                    default:
                        continue;
                }
            } while (!carrierPlaced);
        }

        public bool TakeTurn(Player currentPlayer, Player oppositePlayer)
        {
            bool gameWon = false;
            Console.WriteLine($"{currentPlayer.PlayerName}, prepare to fire!");
            bool badShot = true;
            Coordinate target;
            FireShotResponse response;

            do
            {
                target = SelectCoordinate();
                response = oppositePlayer.PlayBoard.FireShot(target);
                switch (response.ShotStatus)
                {
                    case ShotStatus.Duplicate:
                        Console.WriteLine("You already tried that spot!  Pick another!");
                        Console.ReadLine();
                        break;
                    case ShotStatus.Invalid:
                        Console.WriteLine("You can't shoot outside the board!  Pick another spot!");
                        Console.ReadLine();
                        break;
                    default:
                        badShot = false;
                        break;
                }

            } while (badShot == true);

            switch (response.ShotStatus)
            {
                case ShotStatus.HitAndSunk:
                    Console.WriteLine($"You sank your opponent's {response.ShipImpacted}!");
                    Console.ReadLine();
                    oppositePlayer.DisplayBoard[target.YCoordinate - 1, target.XCoordinate - 1] = 'H';
                    DrawGrid(oppositePlayer.DisplayBoard);
                    break;
                case ShotStatus.Victory:
                    Console.WriteLine($"That's the last ship!  You win!");
                    Console.ReadLine();
                    oppositePlayer.DisplayBoard[target.YCoordinate - 1, target.XCoordinate - 1] = 'H';
                    DrawGrid(oppositePlayer.DisplayBoard);
                    gameWon = true;
                    break;
                case ShotStatus.Hit:
                    Console.WriteLine($"You hit something!");
                    Console.ReadLine();
                    //Console.Read();
                    //update board with red H
                    oppositePlayer.DisplayBoard[target.YCoordinate - 1, target.XCoordinate - 1] = 'H';
                    DrawGrid(oppositePlayer.DisplayBoard);
                    break;
                default: //miss
                    Console.WriteLine("Sploosh!  You didn't hit anything.");
                    Console.ReadLine();
                    //update board with yellow H
                    oppositePlayer.DisplayBoard[target.YCoordinate - 1, target.XCoordinate - 1] = 'M';
                    DrawGrid(oppositePlayer.DisplayBoard);
                    break;
            }
            return gameWon;
        }

        public bool GamePlay()
        {
            bool keepPlaying = true;
            bool gameOver = false;
            int turnCount = 1;
            while (gameOver == false)
            {

                if (turnCount % 2 != 0)
                {
                    Console.WriteLine($"{p1.PlayerName} It's your turn to fire!");
                    DrawGrid(p2.DisplayBoard);
                    gameOver = TakeTurn(p1, p2);
                    turnCount++;
                }
                else
                {
                    Console.WriteLine($"{p2.PlayerName} It's your turn to fire!");
                    DrawGrid(p1.DisplayBoard);
                    gameOver = TakeTurn(p2, p1);
                    turnCount++;
                }
            }
                Console.WriteLine("FINALLY... This arduous game is over! Would you fools like to play again? (Y/N)");
                string answer = Console.ReadLine().ToUpper();
                switch (answer)
                {
                    case "Y":
                        keepPlaying = true;
                        break;
                    default:
                        keepPlaying = false;
                        break;
            }
            return keepPlaying;
        }

        public Coordinate SelectCoordinate()
        {
            bool goodInput = false;
            string x = "";
            string y = "";
            int yCoor = 0;
            int xCoor = 0;
            while (!goodInput)
            {
                Console.WriteLine("Please select a coordinate in the format (A10)");
                string input = Console.ReadLine();
                if (string.IsNullOrEmpty(input))
                {
                    continue;
                }
                x = input.Substring(0, 1).ToUpper();
                y = input.Substring(1, input.Length-1);
                char xChar = char.Parse(x);
                xCoor = xChar - 64;

                if (!(int.TryParse(y, out yCoor)) || yCoor < 1 || yCoor > 10 || xCoor <1 || xCoor >10)
                {
                    Console.WriteLine("That is not a valid coordinate selection.");
                    Console.WriteLine("Please try again.");
                    continue;
                }
                else
                {
                    goodInput = true;
                }
            }
            Coordinate coor = new Coordinate(yCoor, xCoor);
            return coor;
        }
    }
}



