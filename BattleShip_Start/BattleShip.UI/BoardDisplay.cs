﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleShip.UI
{
    public class BoardDisplay
    {
        public char[,] grid = new char[10, 10];

        public static void DrawGrid(char[,] grid)
        {
            Console.Clear();
            string topBar = "  ╔═══╦═══╦═══╦═══╦═══╦═══╦═══╦═══╦═══╦═══╗";
            string bottomBar = "  ╚═══╩═══╩═══╩═══╩═══╩═══╩═══╩═══╩═══╩═══╝";
            Console.WriteLine("\n    A   B   C   D   E   F   G   H   I   J");
            Console.WriteLine(topBar);
            for (int i = 0; i < 10; i++)
            {
                if (i == 0)
                {
                    Console.Write(i);
                }
                if (i > 0)
                {
                    Console.WriteLine($"\n  ╠═══╬═══╬═══╬═══╬═══╬═══╬═══╬═══╬═══╬═══╣");
                    Console.Write(i);
                }
                for (int j = 0; j < 10; j++)
                {
                    Console.Write($" ║{grid[j, i] = ' '} ");
                }
                Console.Write(" ║");
            }
            Console.WriteLine($"\n{bottomBar}");
        }
    }
}
