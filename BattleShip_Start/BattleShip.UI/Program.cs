﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleShip.UI
{
    class Program
    {
        static void Main(string[] args)
        {
            GameManager play = new GameManager();
            play.PlayTheGame();

            Console.ReadLine();
            }
    }
}
