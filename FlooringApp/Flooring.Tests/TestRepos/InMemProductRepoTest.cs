﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flooring.Data.TestRepositories;
using NUnit.Framework;


namespace Flooring.Tests.TestRepos
{
    [TestFixture]
    public class InMemProductRepoTest
    {
        [Test]
        public void TestLoadTestProducts()
        {
            InMemProductRepo repo = new InMemProductRepo();
            var results = repo.LoadProducts();

            Assert.AreEqual(results.Count, 3);
            Assert.AreEqual(results[0].ProductType, "Tile");
            Assert.AreEqual(results[0].ProductCostPerSqFt, 5m);
            Assert.AreEqual(results[0].LaborCostPerSquareFoot, 10m);
            Assert.AreEqual(results[1].ProductType, "Carpet");
            Assert.AreEqual(results[1].ProductCostPerSqFt, 3m);
            Assert.AreEqual(results[1].LaborCostPerSquareFoot, 6m);
            Assert.AreEqual(results[2].ProductType, "Hardwood");
            Assert.AreEqual(results[2].ProductCostPerSqFt, 10m);
            Assert.AreEqual(results[2].LaborCostPerSquareFoot, 8m);
        }
    }
}
