﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flooring.Data.TestRepositories;
using Flooring.Models.Models;
using NUnit.Framework;

namespace Flooring.Tests
{
    [TestFixture]
    public class InMemOrderRepoTest
    {
        [Test]
        public void TestLoadTestOrders()
        {
            InMemOrderRepository repo = new InMemOrderRepository();
            DateTime input = DateTime.Parse("11/11/2016");
            var result = repo.GetOrders(input);

            Assert.AreEqual(result.Count, 1);
            Assert.AreEqual(result[0].CustomerName, "John");
        }

        [Test]
        public void TestAddOrders()
        {
            var date = DateTime.Parse("11/11/16");
            InMemOrderRepository repo = new InMemOrderRepository();
            var order = new Order()
            {
                CustomerName = "Ralph",
                OrderNumber = 3,
                OrderDate = date
            };
            repo.AddOrder(order);
            var orders = repo.GetOrders(date);

            Assert.AreEqual(2, orders.Count);


        }

        [Test]
        public void TestDeleteOrders()
        {
            var date = DateTime.Parse("11/11/16");
            InMemOrderRepository repo = new InMemOrderRepository();
            var orders = repo.GetOrders(date);

            Assert.AreEqual(1, orders.Count);

            repo.DeleteOrder(0, orders[0]);
            var updatedOrders = repo.GetOrders(date);

            Assert.AreEqual(0, updatedOrders.Count);
        }

        [Test]
        public void TestEditOrder()
        {
            InMemOrderRepository repo = new InMemOrderRepository();
            var date = DateTime.Parse("11/11/16");
            var oldOrder = new Order()
            {
                OrderNumber = 1,
                CustomerName = "John",
                OrderDate = DateTime.Parse("11/11/2016"),
                OrderState = new State()
                {
                    StateAbbreviation = "OH",
                    StateName = "Ohio",
                    TaxRate = .08m
                },
                OrderedProduct = new Product()
                {
                    ProductType = "Tile",
                    ProductCostPerSqFt = 5m,
                    LaborCostPerSquareFoot = 10m
                },
                Area = 25m,
                MaterialCost = 125m,
                LaborCost = 250m,
                Tax = 30m,
                Total = 405
            };

            var newOrder = new Order()
            {
                OrderNumber = 1,
                CustomerName = "Thomas",
                OrderDate = DateTime.Parse("11/11/2016"),
                OrderState = new State()
                {
                    StateAbbreviation = "OH",
                    StateName = "Ohio",
                    TaxRate = .08m
                },
                OrderedProduct = new Product()
                {
                    ProductType = "Tile",
                    ProductCostPerSqFt = 5m,
                    LaborCostPerSquareFoot = 10m
                },
                Area = 25m,
                MaterialCost = 125m,
                LaborCost = 250m,
                Tax = 30m,
                Total = 405
            };

            repo.EditAnOrder(newOrder, 0, oldOrder);
            var newListOrders = repo.GetOrders(date);

            Assert.AreEqual("Thomas", newListOrders[0].CustomerName);
        }
    }
}
