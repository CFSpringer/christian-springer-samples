﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flooring.Data.TestRepositories;
using NUnit.Framework;

namespace Flooring.Tests.TestRepos
{
    [TestFixture]
    public class InMemStateRepoTest
    {
        [Test]
        public void TestLoadTestStates()
        {
            InMemStateRepo  repo = new InMemStateRepo();
            var result = repo.LoadStates("OH");

            Assert.AreEqual(result.StateName,"Ohio");
        }
    }
}
