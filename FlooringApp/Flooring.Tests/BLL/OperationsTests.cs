﻿using System;
using System.Runtime.InteropServices;
using Flooring.BLL.Operations;
using Flooring.Models.Models;
using NUnit.Framework;

namespace Flooring.Tests.BLL
{
    [TestFixture]
    public class OperationsTests
    {
        private Order _testOrder = new Order()
        {
            OrderNumber = 1,
            CustomerName = "John",
            OrderDate = DateTime.Parse("11/11/2018"),
            OrderState = new State()
            {
                StateAbbreviation = "OH",
                StateName = "Ohio",
                TaxRate = .08m
            },
            OrderedProduct = new Product()
            {
                ProductType = "Tile",
                ProductCostPerSqFt = 5m,
                LaborCostPerSquareFoot = 10m
            },
            Area = 25m,
            MaterialCost = 125m,
            LaborCost = 250m,
            Tax = 30m
        };

        private Order _testOrder2 = new Order()
        {
            OrderNumber = 1,
            CustomerName = "Robert",
            OrderDate = DateTime.Parse("11/11/2018"),
            OrderState = new State()
            {
                StateAbbreviation = "OH",
                StateName = "Ohio",
                TaxRate = .08m
            },
            OrderedProduct = new Product()
            {
                ProductType = "Tile",
                ProductCostPerSqFt = 5m,
                LaborCostPerSquareFoot = 10m
            },
            Area = 25m,
            MaterialCost = 125m,
            LaborCost = 250m,
            Tax = 30m
        };

        [Test]
        public void TestOrderResponse()
        {
            var response = Operations.GetOrderResponseByDate(DateTime.Parse("01/01/01"));

            Assert.AreEqual(0, response.Orders.Count);
        }

        [Test]
        public void TestRepoForState()
        {
            var state = Operations.CheckRepoForState("CA");

            Assert.AreEqual("CA", state.StateAbbreviation);
        }

        [Test]
        public void TestListOfProducts()
        {
            var products = Operations.GetListOfProducts();

            Assert.AreEqual(3, products.Count);
        }

        [Test]
        public void TestCalculateMaterialCost()
        {
            var result = Operations.CalculateMaterialCost(_testOrder);

            Assert.AreEqual(125m, result);
        }

        [Test]
        public void TestCalculateLaborCost()
        {
            var result = Operations.CalculateLaborCost(_testOrder);

            Assert.AreEqual(250m, result);
        }

        [Test]
        public void TestCalculateCostPerSqFt()
        {
            var result = Operations.CalculateCostPerSqFt(_testOrder);

            Assert.AreEqual(15m, result);
        }

        [Test]
        public void TestCalculateTax()
        {
            var result = Operations.CalculateTaxCost(_testOrder);
            Assert.AreEqual(30m, result);
        }

        [Test]
        public void TestCalculateTotal()
        {
            var result = Operations.CalculateTotal(_testOrder);
            Assert.AreEqual(405m, result);
        }

        [Test]
        public void TestCreateOrderNumber()
        {
            var result = Operations.CreateOrderNumber(_testOrder);

            Assert.AreEqual(1, result.OrderNumber);
        }

        [Test]
        public void TestSaveOrder()
        {
            Operations.SaveOrder(_testOrder);

            var response = Operations.GetOrderResponseByDate(_testOrder.OrderDate);

            Assert.AreEqual(1, response.Orders.Count);
        }

        [Test]
        public void TestDeleteOrder()
        {
            Operations.SaveOrder(_testOrder);

            Operations.DeleteOrder(0, _testOrder);

            var response = Operations.GetOrderResponseByDate(_testOrder.OrderDate);
            Assert.AreEqual(0, response.Orders.Count);
        }

        [Test]
        public void TestEditOrder()
        {
            Operations.SaveOrder(_testOrder);
            Operations.EditOrder(_testOrder2, 0, _testOrder);
            var response = Operations.GetOrderResponseByDate(_testOrder2.OrderDate);

            Assert.AreEqual("Robert", response.Orders[0].CustomerName);
        }
    }
}
