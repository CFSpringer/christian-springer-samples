﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Flooring.Models.Models;
using Flooring.Models.Responses;

namespace Flooring.Data.Interfaces
{
    public interface IOrderRepository
    {
        List<Order> GetOrders(DateTime date);

        void AddOrder(Order orderToAdd);

        void DeleteOrder(int index, Order order);

        void EditAnOrder(Order order, int index, Order oldOrder);
    }
}
