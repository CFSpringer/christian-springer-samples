﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flooring.Models.Models;

namespace Flooring.Data.Interfaces
{
    public interface IStateRepository
    {
        State LoadStates(string stateName);
    }
}
