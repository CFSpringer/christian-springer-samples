﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flooring.Data.Interfaces;
using Flooring.Data.ProductionRepos;
using Flooring.Data.TestRepositories;

namespace Flooring.Data.Factories
{
    public class StateFactory
    {
        public static IStateRepository CreateStateRepository()
        {
            IStateRepository repo;

            string mode = ConfigurationManager.AppSettings["mode"].ToString();

            switch (mode)
            {
                case "test":
                    repo = new InMemStateRepo();
                    break;
                case "production":
                    repo = new ProductionStateRepo();
                    break;
                default:
                    throw new Exception($"{mode} is not recognized");
            }
            return repo;
        }
    }
}
