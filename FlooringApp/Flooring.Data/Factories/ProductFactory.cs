﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flooring.Data.Interfaces;
using Flooring.Data.ProductionRepos;
using Flooring.Data.TestRepositories;

namespace Flooring.Data.Factories
{
    public class ProductFactory
    {
        public static IProductRepository CreateProductRepository()
        {
            IProductRepository repo;

            string mode = ConfigurationManager.AppSettings["mode"].ToString();

            switch (mode)
            {
                case "test":
                    repo = new InMemProductRepo();
                    break;
                case "production":
                    repo = new ProductionProductRepo();
                    break;
                default:
                    throw new Exception($"{mode} is not recognized");
            }
            return repo;
        }
    }
}
