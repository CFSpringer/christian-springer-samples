﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flooring.Data.ProductionRepos
{
    public class ErrorLoggingRepo
    {
        private const string FilePathToLog = @"C:\Users\Apprentice\Documents\SWCGuild\BitBucket\_repos\csspself\Projects\Mastery\Flooring.UI\bin\Debug\DataFiles\ErrorLog.txt";


        public static void WriteToErrorLog(DateTime date, string message)
        {
            using (StreamWriter sw = new StreamWriter(FilePathToLog, true))
            {
                sw.WriteLine($"{date} -- {message}");
            }
        }
    }
}
