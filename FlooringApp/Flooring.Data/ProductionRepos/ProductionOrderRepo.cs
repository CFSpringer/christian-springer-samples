﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flooring.Data.Interfaces;
using Flooring.Models.Models;

namespace Flooring.Data.ProductionRepos
{
    class ProductionOrderRepo : IOrderRepository
    {
        private List<Order> _ordersByDate;

        private const string OrderHeader =
            "OrderNumber,CustomerName,OrderDate,StateAbbreviation,StateName,TaxRate,ProductType,ProductCostPerSqFt,LaborCostPerSquareFoot,Area,MaterialCost,LaborCost,CostPerSquareFoot,Tax,Total";

        private const string directoryPath =
            @"C:\Users\Apprentice\Documents\SWCGuild\BitBucket\_repos\csspself\Projects\Mastery\Flooring.UI\bin\Debug\DataFiles\";


        public List<Order> GetOrders(DateTime date)
        {
            string orderFile = ConvertDateTimeToFileName(date);

            string filePath = CreateAFilePath(orderFile);

            if (File.Exists(filePath))
            {
                List<Order> orders = new List<Order>();
                using (StreamReader sr =
                        File.OpenText($"{filePath}")
                )
                {
                    sr.ReadLine();

                    string inputLine = "";
                    while ((inputLine = sr.ReadLine()) != null)
                    {
                        string[] inputParts = inputLine.Split(',');
                        Order newOrder = new Order()
                        {
                            OrderNumber = int.Parse(inputParts[0]),
                            CustomerName = inputParts[1],
                            OrderDate = DateTime.Parse(inputParts[2]),
                            OrderState = new State()
                            {
                                StateAbbreviation = inputParts[3],
                                StateName = inputParts[4],
                                TaxRate = decimal.Parse(inputParts[5])
                            },
                            OrderedProduct = new Product()
                            {
                                ProductType = inputParts[6],
                                ProductCostPerSqFt = decimal.Parse(inputParts[7]),
                                LaborCostPerSquareFoot = decimal.Parse(inputParts[8])
                            },
                            Area = decimal.Parse(inputParts[9]),
                            MaterialCost = decimal.Parse(inputParts[10]),
                            LaborCost = decimal.Parse(inputParts[11]),
                            CostPerSqFoot = decimal.Parse(inputParts[12]),
                            Tax = decimal.Parse(inputParts[13]),
                            Total = decimal.Parse(inputParts[14])
                        };
                        orders.Add(newOrder);
                    }
                    _ordersByDate = orders;
                    return orders;
                }

            }
            return null;
        }

        public void AddOrder(Order orderToAdd)
        {
            string orderToAddCsv = CreateCsvFromOrder(orderToAdd);

            string fileName = ConvertDateTimeToFileName(orderToAdd.OrderDate);

            string filePath = CreateAFilePath(fileName);
            if (File.Exists(filePath))
            {
                using (StreamWriter sw = new StreamWriter(filePath, true))
                {
                    sw.WriteLine(orderToAddCsv);
                }
            }
            else
            {
                using (StreamWriter sw = new StreamWriter(filePath))
                {
                    sw.WriteLine(OrderHeader);
                    sw.WriteLine(orderToAddCsv);
                }
            }
        }

        public void EditAnOrder(Order order, int index, Order oldOrder)
        {
            //getting a file name for the old date
            string fileName = ConvertDateTimeToFileName(oldOrder.OrderDate);
            //getting a file path for the old order
            string filePath = CreateAFilePath(fileName);
            //loading in the original dates files
            var oldOrders = GetOrders(oldOrder.OrderDate);
            //if the order is staying in the same order date file
            if (order.OrderDate == oldOrder.OrderDate)
            {
                oldOrders[index] = order;
                using (StreamWriter sw = new StreamWriter(filePath))
                {
                    sw.WriteLine(OrderHeader);
                    foreach (var oldOrder1 in oldOrders)
                    {
                        sw.WriteLine(CreateCsvFromOrder(oldOrder1));
                    }
                }
            }
            else
            {
                DeleteOrder(index, oldOrder);

                ////this provides an out in case the order removed was 
                ////the last order in the file
                //if (oldOrders.Count == 0 || oldOrders == null)
                //{
                //    return;
                //}

                ////else write the remaining orders to the same file
                //using (StreamWriter sw = new StreamWriter(filePath))
                //{
                //    sw.WriteLine(OrderHeader);
                //    foreach (var o in oldOrders)
                //    {
                //        string toWrite = CreateCsvFromOrder(o);
                //        sw.WriteLine(toWrite);
                //    }
                //}

                //now add the new order to the new date file
                string newFileName = ConvertDateTimeToFileName(order.OrderDate);

                string newFilePath = CreateAFilePath(newFileName);

                //does the new date already have a file?
                if (File.Exists(newFilePath))
                {
                    //append the new order info to the file
                    using (StreamWriter sw = new StreamWriter(filePath, true))
                    {
                        sw.WriteLine(CreateCsvFromOrder(order));
                    }
                }
                //new date file doesn't exist so make a newONE
                else
                {
                    using (StreamWriter sw = new StreamWriter(newFilePath))
                    {
                        sw.WriteLine(OrderHeader);
                        sw.WriteLine(CreateCsvFromOrder(order));
                    }
                }
            }
        }

        public void DeleteOrder(int index, Order order)
        {
            string fileName = ConvertDateTimeToFileName(order.OrderDate);

            string filePath = CreateAFilePath(fileName);

            var orders = GetOrders(order.OrderDate);
            orders.RemoveAt(index);

            if (orders.Count == 0)
            {
                File.Delete(filePath);
                return;
            }

            using (StreamWriter sw = new StreamWriter(filePath, false))
            {
                sw.WriteLine(OrderHeader);
                foreach (var o in orders)
                {
                    string toWrite = CreateCsvFromOrder(o);
                    sw.WriteLine(toWrite);
                }
            }
        }

        private string ConvertDateTimeToFileName(DateTime date)
        {
            string stringDate = date.ToString("yy-MM-dd");
            string[] dateParts = stringDate.Split('-');
            string orderFileName = ($"order_{dateParts[1]}{dateParts[2]}20{dateParts[0]}.txt");
            return orderFileName;
        }

        private String CreateAFilePath(string fileName)
        {
            string filePath = $"{directoryPath}{fileName}";

            return filePath;
        }

        private string CreateCsvFromOrder(Order order)
        {
            string csv = $"{order.OrderNumber},{order.CustomerName},{order.OrderDate.ToShortDateString()}," +
                         $"{order.OrderState.StateAbbreviation},{order.OrderState.StateName},{order.OrderState.TaxRate}," +
                         $"{order.OrderedProduct.ProductType},{order.OrderedProduct.ProductCostPerSqFt},{order.OrderedProduct.LaborCostPerSquareFoot}," +
                         $"{order.Area},{order.MaterialCost},{order.LaborCost},{order.CostPerSqFoot},{order.Tax},{order.Total}";
            return csv;
        }
    }
}
