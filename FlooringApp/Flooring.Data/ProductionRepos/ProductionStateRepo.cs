﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flooring.Data.Interfaces;
using Flooring.Models.Models;
using System.IO;

namespace Flooring.Data.ProductionRepos
{
    public class ProductionStateRepo  : IStateRepository
    {
        private List<State> _states;

        /// <summary>
        /// This Constructor loads up a list of states from file
        /// </summary>
        public ProductionStateRepo()
        {
            if (_states == null)
            {
                List<State> states = new List<State>();
                using (
                    StreamReader sr =
                        File.OpenText(
                            @"C:\Users\Apprentice\Documents\SWCGuild\BitBucket\_repos\csspself\Projects\Mastery\Flooring.UI\bin\Debug\DataFiles\States.txt")
                )
                {
                    sr.ReadLine();

                    string inputLine = "";
                    while ((inputLine = sr.ReadLine()) != null)
                    {
                        string[] inputParts = inputLine.Split(',');
                        State newState = new State()
                        {
                            StateAbbreviation = inputParts[0],
                            StateName = inputParts[1],
                            //Dividing this by 100  to make an actual tax percentage
                            TaxRate = decimal.Parse(inputParts[2])/100
                        };
                        states.Add(newState);
                    }
                    _states = states;
                }
            }
        }

        /// <summary>
        /// Checks to see if a state exists in the list
        /// </summary>
        /// <param name="stateName">Name or abbreviation on the state</param>
        /// <returns>specific state if it exists in the list</returns>
        public State LoadStates(string stateName)
        {
            foreach (var state in _states)
            {
                if (stateName == state.StateName || stateName == state.StateAbbreviation)
                {
                    return state;
                }
            }
            return null;
        }
    }
}
