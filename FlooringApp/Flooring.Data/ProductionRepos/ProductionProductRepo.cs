﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flooring.Data.Interfaces;
using Flooring.Models.Models;
using System.IO;

namespace Flooring.Data.ProductionRepos
{
    public class ProductionProductRepo  : IProductRepository
    {
        private List<Product> _products;

        public ProductionProductRepo()
        {
            if (_products == null)
            {
                List<Product> products = new List<Product>();
                using (
                    StreamReader sr =
                        File.OpenText(
                            @"C:\Users\Apprentice\Documents\SWCGuild\BitBucket\_repos\csspself\Projects\Mastery\Flooring.UI\bin\Debug\DataFiles\Products.txt")
                )
                {
                    sr.ReadLine();

                    string inputLine = "";
                    while ((inputLine = sr.ReadLine()) != null)
                    {
                        string[] inputParts = inputLine.Split(',');
                        Product newProduct = new Product()
                        {
                            ProductType = inputParts[0],
                            ProductCostPerSqFt = decimal.Parse(inputParts[1]),
                            LaborCostPerSquareFoot = decimal.Parse(inputParts[2])
                        };
                        products.Add(newProduct);
                    }
                }
                _products = products;
            }
        }

        public List<Product> LoadProducts()
        {
            return _products;
        }
    }
}
