﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flooring.Data.Interfaces;
using Flooring.Models.Models;
using Flooring.Models.Responses;

namespace Flooring.Data.TestRepositories
{
    public class InMemOrderRepository : IOrderRepository
    {
        private static List<Order> _orders;

        /// <summary>
        /// Creates a list of two orders complete with state details and product details
        /// </summary>
        public InMemOrderRepository()
        {
            if (_orders == null)
            {
                _orders = new List<Order>
                {
                    new Order()
                    {
                        OrderNumber = 1,
                        CustomerName = "John",
                        OrderDate = DateTime.Parse("11/11/2016"),
                        OrderState = new State()
                        {
                            StateAbbreviation = "OH",
                            StateName = "Ohio",
                            TaxRate = .08m
                        },
                        OrderedProduct = new Product()
                        {
                            ProductType = "Tile",
                            ProductCostPerSqFt = 5m,
                            LaborCostPerSquareFoot = 10m
                        },
                        Area = 25m,
                        MaterialCost = 125m,
                        LaborCost = 250m,
                        Tax = 30m,
                        Total = 405
                    },
                    new Order()
                    {
                        OrderNumber = 1,
                        CustomerName = "Robert",
                        OrderDate = DateTime.Parse("11/12/2016"),
                        OrderState = new State()
                        {
                            StateAbbreviation = "OH",
                            StateName = "Ohio",
                            TaxRate = .08m
                        },
                        OrderedProduct = new Product()
                        {
                            ProductType = "Tile",
                            ProductCostPerSqFt = 5m,
                            LaborCostPerSquareFoot = 10m
                        },
                        Area = 25m,
                        MaterialCost = 125m,
                        LaborCost = 250m,
                        Tax = 30m,
                        Total = 405
                    },
                    new Order()
                    {
                        OrderNumber = 2,
                        CustomerName = "Samuel",
                        OrderDate = DateTime.Parse("11/12/2016"),
                        OrderState = new State()
                        {
                            StateAbbreviation = "OH",
                            StateName = "Ohio",
                            TaxRate = .08m
                        },
                        OrderedProduct = new Product()
                        {
                            ProductType = "Tile",
                            ProductCostPerSqFt = 5m,
                            LaborCostPerSquareFoot = 10m
                        },
                        Area = 25m,
                        MaterialCost = 125m,
                        LaborCost = 250m,
                        Tax = 30m,
                        Total = 405
                    },
                    new Order()
                    {
                        OrderNumber = 3,
                        CustomerName = "Henly",
                        OrderDate = DateTime.Parse("11/12/2016"),
                        OrderState = new State()
                        {
                            StateAbbreviation = "OH",
                            StateName = "Ohio",
                            TaxRate = .08m
                        },
                        OrderedProduct = new Product()
                        {
                            ProductType = "Tile",
                            ProductCostPerSqFt = 5m,
                            LaborCostPerSquareFoot = 10m
                        },
                        Area = 25m,
                        MaterialCost = 125m,
                        LaborCost = 250m,
                        Tax = 30m,
                        Total = 405
                    }
                };
            }
        }

        /// <summary>
        /// gets a list of orders for a specific date
        /// </summary>
        /// <param name="userDateTime">date the user wants</param>
        /// <returns>orders from that date</returns>
        public List<Order> GetOrders(DateTime userDateTime)
        {
            return _orders.Where(o => o.OrderDate == userDateTime).ToList();
        }

        /// <summary>
        /// adds an order to the list of orders
        /// </summary>
        /// <param name="orderToAdd"></param>
        public void AddOrder(Order orderToAdd)
        {
            _orders.Add(orderToAdd);
        }

        public void DeleteOrder(int index, Order order)
        {
            var result = _orders.FirstOrDefault(o => o.OrderDate == (order.OrderDate) && o.OrderNumber == (index + 1));
            _orders.Remove(result);
        }

        public void EditAnOrder(Order order, int index, Order oldOrder)
        {
            var orders = GetOrders(order.OrderDate);
            var oldList = GetOrders(oldOrder.OrderDate);

            if (order.OrderDate == oldOrder.OrderDate)
            {
                oldList[index] = order;
                _orders = oldList;
            }
            else
            {
                _orders.Remove(oldOrder);
                _orders.Add(order);
            }
        }
    }
}
