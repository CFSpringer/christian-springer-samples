﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flooring.Data.Interfaces;
using Flooring.Models.Models;

namespace Flooring.Data.TestRepositories
{
    public class InMemStateRepo  : IStateRepository
    {
        private static List<State> _states;

        /// <summary>
        /// Constructor creates a list of state objects
        /// </summary>
        public InMemStateRepo()
        {
            if (_states == null)
            {
                _states = new List<State>
                {
                    new State()
                    {
                        StateAbbreviation = "OH",
                        StateName = "OHIO",
                        TaxRate = .08m
                    },
                    new State()
                    {
                        StateAbbreviation = "CA",
                        StateName = "CALIFORNIA",
                        TaxRate = .085m
                    },
                    new State()
                    {
                        StateAbbreviation = "OK",
                        StateName = "OKLAHOMA",
                        TaxRate = .072m
                    }
                };
            }
        }

        /// <summary>
        /// Loads up the list of states
        /// </summary>
        /// <returns>a list of state objects</returns>
        public State LoadStates(string stateName)
        {
            foreach (var state in _states)
            {
                if (stateName == state.StateName || stateName == state.StateAbbreviation)
                {
                    return state;
                }
            }
            return null;
        }
    }
}
