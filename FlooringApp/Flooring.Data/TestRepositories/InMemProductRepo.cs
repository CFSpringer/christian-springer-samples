﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flooring.Data.Interfaces;
using Flooring.Models.Models;

namespace Flooring.Data.TestRepositories
{
    public class InMemProductRepo : IProductRepository
    {
        private static List<Product> _products; 

        public InMemProductRepo()
        {
            if (_products == null)
            {
                _products = new List<Product>
                {
                    new Product()
                    {
                        ProductType = "Tile",
                        ProductCostPerSqFt = 5m,
                        LaborCostPerSquareFoot = 10m
                    },
                    new Product()
                    {
                        ProductType = "Carpet",
                        ProductCostPerSqFt = 3m,
                        LaborCostPerSquareFoot = 6m
                    },
                    new Product()
                    {
                        ProductType = "Hardwood",
                        ProductCostPerSqFt = 10m,
                        LaborCostPerSquareFoot = 8m
                    }
                };
            }
        }


        public List<Product> LoadProducts()
        {
            return _products;
        }
    }
}
