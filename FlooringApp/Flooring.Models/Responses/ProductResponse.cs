﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flooring.Models.Models;

namespace Flooring.Models.Responses
{
    public class ProductResponse
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public List<Product> Products { get; set; }
    }
}
