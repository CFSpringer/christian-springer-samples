﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flooring.Models.Models;

namespace Flooring.Models.Responses
{
    public class StateResponse
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public List<State> States { get; set; }
    }
}
