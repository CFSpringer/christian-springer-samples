﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flooring.Data.Factories;
using Flooring.Data.ProductionRepos;
using Flooring.Models.Models;
using Flooring.Models.Responses;

namespace Flooring.BLL.Operations
{
    public class Operations
    {
        /// <summary>
        /// gets an order response from the data layer
        /// </summary>
        /// <param name="userDateTime">date of the orders</param>
        /// <returns>an order response for that date which contains a list if succesful</returns>
        public static OrderResponse GetOrderResponseByDate(DateTime userDateTime)
        {
            OrderResponse response = new OrderResponse();

            var repo = OrderFactory.CreatOrderRepository();

            var source = repo.GetOrders(userDateTime);

            if (source != null)
            {
                response.Success = true;
                response.Orders = source;
            }
            return response;
        }


        /// <summary>
        /// checks to see if the state string passed in is in the repo
        /// </summary>
        /// <param name="input">state name or abbreviation</param>
        /// <returns>a state object if found</returns>
        public static State CheckRepoForState(string input)
        {
            StateResponse response = new StateResponse();

            var repo = StateFactory.CreateStateRepository();

            var state = repo.LoadStates(input);

            return state;
        }


        /// <summary>
        /// reaches into the data layer and gets a list of available products
        /// </summary>
        /// <returns>a list of products</returns>
        public static List<Product> GetListOfProducts()
        {
            var repo = ProductFactory.CreateProductRepository();

            var source = repo.LoadProducts();

            return source;
        }


        public static decimal CalculateMaterialCost(Order order)
        {
            decimal materialCost = (order.Area*order.OrderedProduct.ProductCostPerSqFt);
            return materialCost;
        }


        public static decimal CalculateLaborCost(Order order)
        {
            decimal cost = order.OrderedProduct.LaborCostPerSquareFoot*order.Area;
            return cost;
        }


        public static decimal CalculateCostPerSqFt(Order order)
        {
           return order.CostPerSqFoot = (order.MaterialCost + order.LaborCost)/order.Area;
        }

        
        public static decimal CalculateTaxCost(Order order)
        {
            decimal cost = (order.LaborCost + order.MaterialCost)*order.OrderState.TaxRate;
            return cost;
        }

        
        public static decimal CalculateTotal(Order order)
        {
            decimal total = order.LaborCost + order.MaterialCost + order.Tax;
            return total;
        }

        
        public static Order CalculateTotals(Order order)
        {
            order.MaterialCost = (order.Area * order.OrderedProduct.ProductCostPerSqFt);
            order.LaborCost = order.OrderedProduct.LaborCostPerSquareFoot * order.Area;
            order.CostPerSqFoot = (order.MaterialCost + order.LaborCost) / order.Area;
            order.Tax = (order.LaborCost + order.MaterialCost) * order.OrderState.TaxRate;
            order.Total = order.LaborCost + order.MaterialCost + order.Tax;

            return order;
        }

        /// <summary>
        /// generates a new order number
        /// </summary>
        /// <param name="order">the order that needs a number</param>
        /// <returns>order with an order number</returns>
        public static Order CreateOrderNumber(Order order)
        {
            OrderResponse response = GetOrderResponseByDate(order.OrderDate);

            if (response.Orders == null || response.Orders.Count == 0)
            {
                order.OrderNumber = 1;
            }
            else
            {
                int lastOrderNum = response.Orders[response.Orders.Count - 1].OrderNumber;
                order.OrderNumber = lastOrderNum + 1;
            }
            return order;
        }

        /// <summary>
        /// reaches to the data layer and calls the save method
        /// </summary>
        /// <param name="order">the order object to save</param>
        public static void SaveOrder(Order order)
        {
            var repo = OrderFactory.CreatOrderRepository();

            repo.AddOrder(order);

        }

        /// <summary>
        /// reaches to data and calls the delete method
        /// </summary>
        /// <param name="index">The orders location in the list</param>
        /// <param name="order">order  to delete</param>
        public static void DeleteOrder(int index, Order order)
        {
            var repo = OrderFactory.CreatOrderRepository();

            repo.DeleteOrder(index, order);

            var updatedRepo = OrderFactory.CreatOrderRepository().GetOrders(order.OrderDate);

            if (updatedRepo == null)
            {
                return;
            }

            if (updatedRepo.Count == 1)
            {
                updatedRepo[0].OrderNumber = 1;
                return;
            }
            for (int i = 0; i < updatedRepo.Count; i++)
            {
                updatedRepo[i].OrderNumber = i + 1;
            }
        }

        /// <summary>
        /// reaches  to data and calls the edit order method
        /// </summary>
        /// <param name="order">updated order</param>
        /// <param name="index">orders location in the original list</param>
        /// <param name="oldOrder">original order information used for comparing dates</param>
        public static void EditOrder(Order order, int index, Order oldOrder)
        {
            var repo = OrderFactory.CreatOrderRepository();
            order = Operations.CreateOrderNumber(order);
            repo.EditAnOrder(order, index, oldOrder);
            
            
        }

        /// <summary>
        /// calls the error log in data to write the date/time and a message
        /// </summary>
        /// <param name="date">date time now of an error</param>
        /// <param name="message">message to be displayed</param>
        public static void WriteError(DateTime date, string message)
        {
            ErrorLoggingRepo.WriteToErrorLog(date, message);
        }
    }
}
