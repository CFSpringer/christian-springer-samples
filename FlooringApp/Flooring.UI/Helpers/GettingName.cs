﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flooring.UI.Helpers
{
    public class GettingName
    {
        /// <summary>
        /// gets a user name that is not null or whitspace
        /// </summary>
        /// <returns>string name</returns>
        public static string GetUserName()
        {
            string input = "";
            while (string.IsNullOrWhiteSpace(input))
            {
                Console.Clear();
                Console.Write("Enter a name for the order: ");
                input = Console.ReadLine();
            }
            return input;
        }
    }
}

