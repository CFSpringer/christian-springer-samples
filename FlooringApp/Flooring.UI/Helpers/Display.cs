﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flooring.BLL.Operations;
using Flooring.Models.Models;
using Flooring.Models.Responses;

namespace Flooring.UI.Helpers
{
    public class Display
    {
        /// <summary>
        /// prints out order details
        /// </summary>
        /// <param name="order">the order which is going to be printed</param>
        public static void DisplayOrder(Order order)
        {
            Console.Clear();
            Console.WriteLine($"Order Details\n{ConsoleIO.HeaderBar}");
            Console.WriteLine($"Order Number: {order.OrderNumber}");
            Console.WriteLine($"Order Date: {order.OrderDate.ToShortDateString()}");
            Console.WriteLine($"Customer Name: {order.CustomerName}");
            Console.WriteLine($"State: {order.OrderState.StateName}");
            Console.WriteLine($"Product Type: {order.OrderedProduct.ProductType}");
            Console.WriteLine($"Cost per Square Foot: {order.OrderedProduct.ProductCostPerSqFt:c}");
            Console.WriteLine($"Labor Cost per Square Foot: {order.OrderedProduct.LaborCostPerSquareFoot:c}");
            Console.WriteLine($"Total Area: {order.Area} Square Feet");
            Console.WriteLine($"Material Cost {order.MaterialCost:c}");
            Console.WriteLine($"Labor Cost: {order.LaborCost:c}");
            Console.WriteLine($"Cost per Square Foot: {order.CostPerSqFoot:c}");
            Console.WriteLine($"Tax: {order.Tax:c}");
            Console.WriteLine($"Total: {order.Total:c}");
        }

        /// <summary>
        /// Goes through the list of orders in Order Response to view
        /// </summary>
        /// <param name="response">OrderResponse Contains list of orders </param>
        public static void DisplayListOfOrders(OrderResponse response)
        {
            while (true)
            {

                if (response.Orders == null || response.Orders.Count == 0)
                {
                    Console.WriteLine("No Orders found for that date.");
                    Operations.WriteError(DateTime.Now, "No Orders found for the requested date.");
                    return;
                }
                for (int i = 0; i < response.Orders.Count; i++)
                {
                    Console.WriteLine($" {i + 1}. Customer Name: {response.Orders[i].CustomerName}");
                    Console.WriteLine($"\tProduct Type: {response.Orders[i].OrderedProduct.ProductType}");

                    Console.WriteLine($"\tTotal: {response.Orders[i].Total:c}");
                }
                Console.Write("Select an order to view or Q to quit: ");
                string selection = Console.ReadLine().ToUpper();
                if (selection == "Q")
                {
                    return;
                }
                int num;
                if (int.TryParse(selection, out num))
                {
                    if (num < 1 || num > response.Orders.Count)
                    {
                        Console.WriteLine("Input Error.");
                        Console.WriteLine("Enter the number of the order to view or Q to quit.");
                        Console.WriteLine("Press any key to continue...");
                        Operations.WriteError(DateTime.Now, "User selected an index out of range for the orders on this day.");
                        Console.ReadKey();
                        continue;
                    }
                    else
                    {
                        Display.DisplayOrder(response.Orders[num - 1]);
                        return;
                    }
                }
                else
                {
                    Console.WriteLine("Input Error.");
                    Console.WriteLine("Enter the number of the order to view or Q to quit.");
                    Console.WriteLine("Press any key to continue...");
                    Operations.WriteError(DateTime.Now, "User selected an index out of range for the orders on this day.");
                    Console.ReadKey();
                    continue;
                }
            }
        }

        /// <summary>
        /// displays a list of products
        /// </summary>
        public static void DisplayProducts()
        {
            var products = Operations.GetListOfProducts();
            Console.Clear();
            Console.WriteLine("Products");
            Console.WriteLine($"{ConsoleIO.HeaderBar}");
            for (int i = 0; i < products.Count; i++)
            {
                Console.WriteLine($"{i+1}. {products[i].ProductType}");
                Console.WriteLine($"\t{products[i].ProductCostPerSqFt:c} - Material Cost per Square Foot");
                Console.WriteLine($"\t{products[i].LaborCostPerSquareFoot:c} - Labor Cost per Square Foot");
            }
        }

        
    }
}
