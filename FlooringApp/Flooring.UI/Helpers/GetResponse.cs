﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flooring.BLL.Operations;
using Flooring.Models.Responses;

namespace Flooring.UI.Helpers
{
    public class GetResponse
    {
        /// <summary>
        /// Retrieves an order Response
        /// </summary>
        /// <param name="userDateTime">User specified date</param>
        /// <returns>orderResponse object </returns>
        public static OrderResponse GetOrderResponse(DateTime userDateTime)
        {
            Console.Clear();
            var response = Operations.GetOrderResponseByDate(userDateTime);
            if (!response.Success)
            {
                Console.WriteLine($"{response.Message}");
                Console.WriteLine("Press any key to return to the main menu...");
            }
            return response;
        }

    }
}
