﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flooring.UI.Helpers
{
    public static class GetDate
    {
        /// <summary>
        /// Gets a valid DateTime Object from the user
        /// </summary>
        /// <returns>A valid DateTime Object</returns>
        public static DateTime GetDateFromUser(string prompt)
        {
            bool notValidDate = true;
            DateTime userDateTime;
            do
            {
                Console.Clear();
                Console.WriteLine(prompt);
                string input = Console.ReadLine();
                if (DateTime.TryParse(input, out userDateTime))
                {
                    notValidDate = false;
                }
                else
                {
                    Console.WriteLine("That is not a valid date.");
                    Console.WriteLine("Press any key to continue...");
                    Console.ReadKey();
                }
            } while (notValidDate);
            return userDateTime;
        }
    }
}
