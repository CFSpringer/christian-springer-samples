﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Flooring.BLL.Operations;
using Flooring.Models.Models;
using Flooring.Models.Responses;
using Flooring.UI.Helpers;

namespace Flooring.UI.Workflows
{
    public class AddAnOrderWorkflow
    {
        public static void Execute()
        {
            Console.Clear();
            DateTime userDate = GetDate.GetDateFromUser("Enter a date for this order");
            var newOrder = MakeNewOrder();
            newOrder.OrderDate = userDate;
            newOrder.CustomerName = GettingName.GetUserName();
            newOrder = Operations.CreateOrderNumber(newOrder);
            newOrder.OrderedProduct = GetProductFromUser();
            newOrder.OrderState = GetStateFromUser();
            newOrder.Area = GetAreaFromUser();
            newOrder = Operations.CalculateTotals(newOrder);
            ConfirmOrder(newOrder);
            Console.ReadKey();
        }

        /// <summary>
        /// makes a new order to be populated by the user
        /// </summary>
        /// <returns>a fresh order object</returns>
        public static Order MakeNewOrder()
        {
            Order newOrder = new Order();
            return newOrder;
        }

        /// <summary>
        /// asks the user to select a product from a list of products
        /// </summary>
        /// <returns>a specific product object</returns>
        private static Product GetProductFromUser()
        {
            Console.Clear();
            Console.WriteLine("Select the desired product from the list.");
            Console.WriteLine(ConsoleIO.HeaderBar);
            var products = Operations.GetListOfProducts();
            Console.WriteLine("Select a product type from the list below");
            while (true)
            {
                Console.Clear();
                Display.DisplayProducts();
                string input = Console.ReadLine().ToUpper();
                if (string.IsNullOrWhiteSpace(input))
                {
                    Console.WriteLine("Please enter a selection.");
                    Console.WriteLine("Press any key to continue...");
                    Operations.WriteError(DateTime.Now, "User did not enter a product into the select a product menu");
                    Console.ReadKey();
                    continue;
                }
                int index;
                if (int.TryParse(input, out index))
                {
                    if (index < 1 || index > products.Count)
                    {
                        Console.WriteLine("That is not a valid selection.");
                        Console.WriteLine("Press any key to continue...");
                        Operations.WriteError(DateTime.Now, "User did not make a valid product selection.");
                        Console.ReadKey();
                        continue;
                    }
                }
                return products[index - 1];
            }
        }

        /// <summary>
        /// gets a state from the user
        /// </summary>
        /// <returns>specific state object</returns>
        public static State GetStateFromUser()
        {
            while (true)
            {
                Console.Clear();
                Console.Write("Type the full name or the abbreviate of your state: ");
                string input = Console.ReadLine().ToUpper();
                var newState = Operations.CheckRepoForState(input);
                if (newState == null)
                {
                    Console.WriteLine("That state does not exist in our directory");
                    Console.WriteLine("Press any key to continue...");
                    Operations.WriteError(DateTime.Now, "The state could not be found in our directory.");
                    Console.ReadKey();
                    continue;
                }
                return newState;

            }
        }

        /// <summary>
        /// gets an area greater than zero
        /// </summary>
        /// <returns>decimal value for area</returns>
        private static decimal GetAreaFromUser()
        {
            decimal area;
            while (true)
            {
                Console.Clear();
                Console.Write($"Enter the area of the room: ");
                string input = Console.ReadLine();
                if (decimal.TryParse(input, out area))
                {
                    if (area < 0)
                    {
                        continue;
                    }
                    return area;
                }
                else
                {
                    Console.WriteLine("That is not a valid selection.");
                    Console.WriteLine("Press any key to continue..");
                    Operations.WriteError(DateTime.Now, "User did not give a valid decimal greater than zero for the area.");
                    Console.ReadKey();
                }
            }
        }

        /// <summary>
        /// asks  the BLL to calulate material cost
        /// </summary>
        /// <param name="order">the order that needs calculated</param>
        /// <returns>decimal value for  material cost</returns>
        private static decimal GetMaterialCost(Order order)
        {
            decimal cost = Operations.CalculateMaterialCost(order);
            return cost;
        }

        private static decimal GetLaborCost(Order order)
        {
            decimal cost = Operations.CalculateLaborCost(order);
            return cost;
        }

        private static decimal GetCostPerSqFt(Order order)
        {
            return Operations.CalculateCostPerSqFt(order);
        }

        private static decimal GetTax(Order order)
        {
            decimal tax = Operations.CalculateTaxCost(order);
            return tax;
        }

        private static decimal GetTotal(Order order)
        {
            decimal total = Operations.CalculateTotal(order);
            return total;
        }

        /// <summary>
        /// confirms that the user wants to save an order
        /// </summary>
        /// <param name="order">order to be saved</param>
        private static void ConfirmOrder(Order order)
        {
            while (true)
            {
                Console.Clear();
                Display.DisplayOrder(order);
                Console.Write("Save this order (Y/N)?");
                string input = Console.ReadLine().ToUpper();
                switch (input)
                {
                    case "Y":
                        Operations.SaveOrder(order);
                        Console.WriteLine("Order Saved");
                        break;
                    case "N":
                        Console.WriteLine("Order Discarded");
                        break;
                    default:
                        Console.WriteLine("Enter either Y to save or N to cancel");
                        Console.WriteLine("Press any key to continue");
                        Console.ReadKey();
                        Operations.WriteError(DateTime.Now, "User did not select either 'Y' or 'N' to save or discard order");
                        continue;
                }
                return;
            }
        }
    }
}
