﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Flooring.BLL.Operations;
using Flooring.Models.Models;
using Flooring.Models.Responses;
using Flooring.UI.Helpers;

namespace Flooring.UI.Workflows
{
    public class DisplayAnOrderWorkflow
    {
        public static void Execute()
        {
            Console.Clear();

            var userDate = GetDate.GetDateFromUser("Enter a date for the order (mm/dd/yyyy)");
            var response = GetResponse.GetOrderResponse(userDate);
            Display.DisplayListOfOrders(response);
            Console.WriteLine("\nPress any key to continue...");
            Console.ReadKey();
        }
    }
}
    