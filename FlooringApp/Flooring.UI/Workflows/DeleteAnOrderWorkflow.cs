﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flooring.BLL.Operations;
using Flooring.Models.Models;
using Flooring.UI.Helpers;
using Flooring.Models.Responses;

namespace Flooring.UI.Workflows
{
    public class DeleteAnOrderWorkflow
    {
        public static void Execute()
        {
            //get date from user
            DateTime deleteDate = GetDate.GetDateFromUser("Enter the date of the order you would like to delete.");
            //get order  response for that date
            var response = GetResponse.GetOrderResponse(deleteDate);
            Console.WriteLine($"Orders from {deleteDate.ToShortDateString()}");
            //display the orders for that date and get the index of the desired order
            int index = DisplayListOfOrders(response);
            if (index == -1)
            {
                return;
            }
            //confirm the user wants to delete the order
            ConfirmDeletion(response.Orders[index], index);
        }

        private static void ConfirmDeletion(Order order, int index)
        {
            while (true)
            {
                Console.Clear();
                Display.DisplayOrder(order);
                Console.Write("Are you sure you want to delete (Y/N)?");
                string input = Console.ReadLine().ToUpper();
                switch (input)
                {
                    case "Y":
                        Operations.DeleteOrder(index, order);
                        Console.WriteLine("Order Discarded");
                        Console.WriteLine("Press any key to continue...");
                        Console.ReadKey();
                        break;
                    case "N":
                        Console.WriteLine("Order Kept");
                        break;
                    default:
                        Console.WriteLine("Enter either Y to save or N to cancel");
                        Console.WriteLine("Press any key to continue");
                        Operations.WriteError(DateTime.Now, "User did not select either 'Y' orr 'N' to delete an order.");
                        Console.ReadKey();
                        continue;
                }
                return;
            }
        }

        private static int DisplayListOfOrders(OrderResponse response)
        {
            if (response.Orders.Count == 0)
            {
                Console.WriteLine("No Orders found for that date.");
                Console.WriteLine("Press any key to continue...");
                Console.ReadKey();
                return -1;
            }
            while (true)
            {
                for (int i = 0; i < response.Orders.Count; i++)
                {
                    Console.WriteLine($" {i + 1}. Customer Name: {response.Orders[i].CustomerName}");
                    Console.WriteLine($"\tProduct Type: {response.Orders[i].OrderedProduct.ProductType}");
                    Console.WriteLine($"\tTotal: {response.Orders[i].Total:c}");
                }
                Console.Write("Select an order to Delete or Q to quit: ");
                string selection = Console.ReadLine().ToUpper();
                if (selection == "Q")
                {
                    return -1;
                }
                int num;
                if (int.TryParse(selection, out num))
                {
                    if (num < 1 || num > response.Orders.Count)
                    {
                        Console.WriteLine("Input Error.");
                        Console.WriteLine("Enter the number of the order to view or Q to quit.");
                        Console.WriteLine("Press any key to continue...");
                        Console.ReadKey();
                        continue;
                    }
                    else
                    {
                        Display.DisplayOrder(response.Orders[num - 1]);
                        return num -1;
                    }
                }
                else
                {
                    Console.WriteLine("Input Error.");
                    Console.WriteLine("Enter the number of the order to view or Q to quit.");
                    Console.WriteLine("Press any key to continue...");
                    Console.ReadKey();
                    continue;
                }
            }
        }
    }
}
