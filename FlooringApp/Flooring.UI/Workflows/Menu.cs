﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Flooring.BLL.Operations;
using Flooring.UI.Workflows;

namespace Flooring.UI
{
    public class Menu
    {
        /// <summary>
        /// displays the main menu
        /// </summary>
        public static void DisplayStart()
        {
            string input = "";
            do
            {

                Console.Clear();

                Console.WriteLine($"Master Flooring\n{ConsoleIO.HeaderBar}");
                Console.WriteLine("1. Display Order");
                Console.WriteLine("2. Add Order");
                Console.WriteLine("3. Edit Order");
                Console.WriteLine("4. Delete Order");
                Console.WriteLine($"\n{ConsoleIO.HeaderBar}\n");
                Console.WriteLine("Press Q to Quit");

                input = Console.ReadLine().ToUpper();
                if (input != "Q")
                {
                    ProcessInput(input);
                }
            } while (input != "Q")
                ;

        }

        /// <summary>
        /// processes the users choice by calling the correct worflow
        /// </summary>
        /// <param name="input">user choice</param>
        public static void ProcessInput(string input)
        {
            switch (input)
            {
                case "1":
                    Console.WriteLine("Display an Order");
                    DisplayAnOrderWorkflow.Execute();
                    break;
                case "2":
                    AddAnOrderWorkflow.Execute();
                    break;
                case "3":
                    EditAnOrderWorkflow.Execute();
                    break;
                case "4":
                    DeleteAnOrderWorkflow.Execute();
                    break;
                default:
                    Console.WriteLine($"{input} is not a valid selection.");
                    Console.WriteLine("Press any key to continue...");
                    Operations.WriteError(DateTime.Now, "User made an invalid selection at the Main Menu.");
                    Console.ReadKey();
                    return;
            }

        }
    }
}