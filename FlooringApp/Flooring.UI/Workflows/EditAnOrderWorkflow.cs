﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Flooring.BLL.Operations;
using Flooring.Models.Models;
using Flooring.Models.Responses;
using Flooring.UI.Helpers;

namespace Flooring.UI.Workflows
{
    public class EditAnOrderWorkflow : AddAnOrderWorkflow
    {
        public static void Execute()
        {
            //gets the date of the order that needs edited
            DateTime dateToEdit = GetDate.GetDateFromUser("Enter the date of the order you want to edit");
            var response = GetResponse.GetOrderResponse(dateToEdit);
            int index = DisplayListOfOrders(response);
            if (index < 0)
            {
                return;
            }
            var duplicateOrder = CopyOriginalOrder(response, index);
            duplicateOrder = EditCustomerName(duplicateOrder);
            duplicateOrder = EditOrderDate(duplicateOrder);
            duplicateOrder = EditState(duplicateOrder);
            duplicateOrder = EditAreaForProduct(duplicateOrder);
            duplicateOrder.Tax = Operations.CalculateTaxCost(duplicateOrder);
            duplicateOrder.Total = Operations.CalculateTotal(duplicateOrder);
            duplicateOrder = EditOrderProduct(duplicateOrder);
            duplicateOrder = Operations.CalculateTotals(duplicateOrder);
            ConfirmEdits(duplicateOrder, index, response.Orders[index]);

            Console.ReadKey();
        }

        private static int DisplayListOfOrders(OrderResponse response)
        {
            while (true)
            {
                for (int i = 0; i < response.Orders.Count; i++)
                {
                    Console.WriteLine($" {i + 1}. Customer Name: {response.Orders[i].CustomerName}");
                    Console.WriteLine($"\tProduct Type: {response.Orders[i].OrderedProduct.ProductType}");
                    Console.WriteLine($"\tTotal: {response.Orders[i].Total:c}");
                }
                Console.Write("Select an order to Edit or Q to quit: ");
                string selection = Console.ReadLine().ToUpper();
                if (selection == "Q")
                {
                    return -1;
                }
                int num;
                if (int.TryParse(selection, out num))
                {
                    if (num < 1 || num > response.Orders.Count)
                    {
                        Console.WriteLine("Input Error.");
                        Console.WriteLine("Enter the number of the order to view or Q to quit.");
                        Console.WriteLine("Press any key to continue...");
                        Console.ReadKey();
                        Operations.WriteError(DateTime.Now, "User did not make a selection for an order to view.");
                        continue;
                    }
                    else
                    {
                        Display.DisplayOrder(response.Orders[num - 1]);
                        return num - 1;
                    }
                }
                else
                {
                    Console.WriteLine("Input Error.");
                    Console.WriteLine("Enter the number of the order to view or Q to quit.");
                    Console.WriteLine("Press any key to continue...");
                    Operations.WriteError(DateTime.Now, "User did not make a selection for an order to view.");
                    Console.ReadKey();
                    continue;
                }
            }
        }

        /// <summary>
        /// makes a duplicate of an order
        /// </summary>
        /// <param name="response">original order response</param>
        /// <param name="index">the location of the order in the response list</param>
        /// <returns> a duplicate of an order</returns>
        private static Order CopyOriginalOrder(OrderResponse response, int index)
        {
            var duplicateOrder = AddAnOrderWorkflow.MakeNewOrder();
            duplicateOrder.CustomerName = response.Orders[index].CustomerName;
            duplicateOrder.OrderNumber = response.Orders[index].OrderNumber;
            duplicateOrder.OrderDate = response.Orders[index].OrderDate;
            duplicateOrder.OrderState = response.Orders[index].OrderState;
            duplicateOrder.OrderedProduct = response.Orders[index].OrderedProduct;
            duplicateOrder.Area = response.Orders[index].Area;
            duplicateOrder.MaterialCost = response.Orders[index].MaterialCost;
            duplicateOrder.LaborCost = response.Orders[index].LaborCost;
            duplicateOrder.CostPerSqFoot = response.Orders[index].CostPerSqFoot;
            duplicateOrder.Tax = response.Orders[index].Tax;
            duplicateOrder.Total = response.Orders[index].Total;

            return duplicateOrder;
        }


        private static Order EditCustomerName(Order order)
        {
            Console.Clear();
            Display.DisplayOrder(order);
            Console.WriteLine(ConsoleIO.HeaderBar);
            Console.WriteLine("Enter a new name press enter to continue with the current name.");
            Console.Write($"Current Customer Name {order.CustomerName} ");
            string input = Console.ReadLine();
            if (string.IsNullOrWhiteSpace(input))
            {
                return order;
            }
            else
            {
                order.CustomerName = input;
                return order;
            }

        }

        private static Order EditOrderDate(Order order)
        {
            while (true)
            {
                DateTime newDate;
                Console.Clear();
                Display.DisplayOrder(order);
                Console.WriteLine(ConsoleIO.HeaderBar);
                Console.WriteLine("Enter a new date or press enter to continue with the current name.");
                Console.Write($"Current Order Date {order.OrderDate.ToShortDateString()} ");
                string input = Console.ReadLine();
                if (string.IsNullOrWhiteSpace(input))
                {
                    return order;
                }
                if (DateTime.TryParse(input, out newDate))
                {
                    order.OrderDate = newDate;
                    return order;
                }
                else
                {
                    Console.WriteLine("That is not a valid date.");
                    Console.WriteLine("Press any key to continue...");
                    Operations.WriteError(DateTime.Now, "User entered an invalid date.");
                    Console.ReadKey();
                    continue;
                }
            }
        }

        private static Order EditState(Order order)
        {
            while (true)
            {
                Console.Clear();
                Display.DisplayOrder(order);
                Console.WriteLine(ConsoleIO.HeaderBar);
                Console.WriteLine($"Current State: {order.OrderState.StateName}");
                Console.Write("Enter a new state or press enter to keep the current state: ");
                string input = Console.ReadLine().ToUpper();
                if (string.IsNullOrWhiteSpace(input))
                {
                    return order;
                }
                var newState = Operations.CheckRepoForState(input);
                if (newState == null)
                {
                    Console.WriteLine("That state does not exist in our directory");
                    Console.WriteLine("Press any key to continue...");
                    Operations.WriteError(DateTime.Now, "The state could not be found in our directory.");
                    Console.ReadKey();
                    continue;
                }
                 order.OrderState = newState;
                return order;
            }
        }

        private static Order EditOrderProduct(Order order)
        {
            while (true)
            {
                Console.Clear();
                Display.DisplayOrder(order);
                Console.WriteLine(ConsoleIO.HeaderBar);
                var products = Operations.GetListOfProducts();
                Display.DisplayProducts();
                Console.WriteLine($"Current Product: { order.OrderedProduct.ProductType}");
                Console.Write("Select a new product type from the list below or press enter ");
                string input = Console.ReadLine();
                if (string.IsNullOrWhiteSpace(input))
                {
                    return order;
                }
                int index;
                if (int.TryParse(input, out index))
                {
                    if (index < 1 || index > products.Count)
                    {
                        Console.WriteLine("That is not a valid selection.");
                        Console.WriteLine("Press any key to continue...");
                        Operations.WriteError(DateTime.Now, "User did not make a valid product selection.");
                        Console.ReadKey();
                        continue;
                    }
                }
                order.OrderedProduct = products[index - 1];
                return order;
            }
        }

        private static Order EditAreaForProduct(Order order)
        {
            while (true)
            {
                Console.Clear();
                Display.DisplayOrder(order);
                Console.WriteLine(ConsoleIO.HeaderBar);
                Console.WriteLine("Enter a new area press enter to continue with the current area.");
                Console.Write($"Current Area {order.Area} sq. Feet ");
                string input = Console.ReadLine();
                decimal area;
                if (string.IsNullOrWhiteSpace(input))
                {
                    return order;
                }
                if (decimal.TryParse(input, out area))
                {
                    order.Area =  area;
                    return order;
                }
                else
                {
                    Console.WriteLine("That is not a valid selection.");
                    Console.WriteLine("Press any key to continue..");
                    Operations.WriteError(DateTime.Now, "User did not enter a valid decimal for area greater than zero.");
                    Console.ReadKey();
                }
            }
        }

        /// <summary>
        /// Confirms that the user wants to retain the edits
        /// </summary>
        /// <param name="order">edited order</param>
        /// <param name="index">original loccation of order in list</param>
        /// <param name="oldOrder">old order  info</param>
        private static void ConfirmEdits(Order order, int index, Order oldOrder)
        {
            while (true)
            {
                Console.Clear();
                Display.DisplayOrder(order);
                Console.Write("Save this order (Y/N)?");
                string input = Console.ReadLine().ToUpper();
                switch (input)
                {
                    case "Y":
                        Operations.EditOrder(order, index, oldOrder);
                        Console.WriteLine("Order Updated");
                        break;
                    case "N":
                        Console.WriteLine("Edits Discarded");
                        break;
                    default:
                        Console.WriteLine("Enter either Y to save or N to cancel");
                        Console.WriteLine("Press any key to continue");
                        Console.ReadKey();
                        Operations.WriteError(DateTime.Now, "User did not enter either 'Y' or 'N' to confirm edits");
                        continue;
                }
                return;
            }
        }
    }
}
