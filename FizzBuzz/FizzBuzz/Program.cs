﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int i = 1; i <= 100; i++)
            {
                string fizz = "";
                string buzz = "";
                if (i % 3 == 0)
                {
                    fizz = "Fizz";
                }
                if (i % 5 == 0)
                {
                    buzz = "Buzz";
                }
                Console.WriteLine($"{i} {fizz}{buzz}");
            }
            Console.Read();
        }
    }
}
