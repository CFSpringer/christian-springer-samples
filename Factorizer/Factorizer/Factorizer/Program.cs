﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Factorizer
{
    public class Program
    {
        static void Main(string[] args)
        {
            Program start = new Program();
            //if !0 CONTINUE ELSE IM OUTTA HERE
            int userNumber = start.Greeting();
            if (userNumber != 0)
            {
                List<int> factors = start.GetFactors(userNumber);
                Console.WriteLine($"The factors of {userNumber} are:");
                int sum = factors.Sum();
                foreach (int factor in factors)
                {
                    Console.WriteLine($"{factor}\n");
                }

                if (sum == userNumber)
                {
                    Console.WriteLine($"{userNumber} is a perfect number. \n");
                    Console.WriteLine($"{userNumber} is not a prime number");
                    Console.ReadLine();
                }
                else if (sum == 1)
                {
                    Console.WriteLine($"{userNumber} is not perfect number.\n");
                    Console.WriteLine($"{userNumber} is a prime number");
                    Console.ReadLine();
                }
                else
                {
                    Console.WriteLine($"{userNumber} is not perfect number.\n");
                    Console.WriteLine($"{userNumber} is not prime number");
                    Console.ReadLine();
                }
            }

            else
            {
                return;
            }
        }


        




        public int Greeting()
        {
            Console.WriteLine("Welcome to the FACTORIZOR!\n"); 
            int inputInt = 0;
            bool isValidInt;

            do
            {
                Console.Write("Please enter a number you would like to factor or zero to quit: ");
                string input = Console.ReadLine();
                isValidInt = int.TryParse(input, out inputInt);
            }
            while (!isValidInt);

            return inputInt;
        }




        public List<int> GetFactors(int num)
        {
            List<int> a = new List<int>();
            for (int i = 1; i < num; i++)
            {
                int factor = num % i;

                if (factor == 0)
                {
                    a.Add(i);
                }
            }
            return a;
        } 
    }
}
