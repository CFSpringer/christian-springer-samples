﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace FactorizerClassTest
{
    [TestFixture]
    public class FactorizerClassTests
    {
        [TestCase("1", 1)]
        [TestCase("20", 20)]
        [TestCase("0", 0)]
        public static void GreetingTest(string a, int expected)
        {
            FactorizerClass ft = new FactorizerClass();
            int result = ft.Greeting(a);
            Assert.AreEqual(expected, result);
        }

        [TestCase(4)]
        public static void GetFactorsTest(int a)
        {
            FactorizerClass ft = new FactorizerClass();
            List<int> result = ft.GetFactors(a);

            List<int> expected = new List<int>();
            expected.Add(1);
            expected.Add(2);

            CollectionAssert.AreEqual(expected, result);
        }

        [TestCase(10)]
        public static void GetFactorsTest1(int a)
        {
            FactorizerClass ft = new FactorizerClass();
            List<int> result = ft.GetFactors(a);

            List<int> expected = new List<int>();
            expected.Add(1);
            expected.Add(2);
            expected.Add(5);

            CollectionAssert.AreEqual(expected, result);
        }




       /* [TestCase(10, "1,2,5")]
        [TestCase(4, "1,2,4")]
        public static void GetFactors2(int a , string csvnumbers )
        {
            FactorizerClass ft = new FactorizerClass();
            bool allEqual = true;
            List<int> result = ft.GetFactors(a);

            if(result.Count!= )
            string[] aNumbers = csvnumbers.Split(',');
            foreach (var aNum in aNumbers)
            {
                if (!result.Contains(int.Parse(aNum)))
                {
                    allEqual = false;
                    break;
                }
            }
            Assert.AreEqual(true, allEqual);
        }*/

    }
}

