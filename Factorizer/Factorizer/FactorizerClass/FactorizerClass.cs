﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace FactorizerClassTest
{
    public class FactorizerClass
    {
        //Class to get a  string from a user and convert it to an int with validation to ensure the number entered
        //is an "parsable"
        public int Greeting(string a)
        {
            //Program start = new Program();
            bool isValidInt;
            int inputInt = 0;
            do
            {
                isValidInt = int.TryParse(a, out inputInt);
            }
            while (!isValidInt);

            return inputInt;
        }
        public List<int> GetFactors(int num)
        {
            List<int> a = new List<int>();
            for (int i = 1; i < num; i++)
            {
                int factor = num % i;

                if (factor == 0)
                {
                    a.Add(i);
                }
            }
            return a;
        }


        //need a function that will add all of the items in the array list and determine if the number is perfect or prime.
    }
}

